from flask import Flask, render_template, url_for
from flask_sqlalchemy import Pagination, SQLAlchemy
import json
import os

app = Flask(__name__)
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///database.db"
db = SQLAlchemy(app)

class Games(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String)
    icon = db.Column(db.String)
    description = db.Column(db.String)
    resources = db.Column(db.String)
    developer_name = db.Column(db.String)
    screenshots = db.Column(db.String)
    category_id = db.Column(db.Integer, db.ForeignKey('category.id'), nullable=False)
    category = db.relationship('Category',backref=db.backref('games', lazy='dynamic'))

class Category(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String)

root_path = os.getcwd()
jdata = root_path + "/app.json"

def update_db(jdata):
    with open(jdata, "r") as d:
        data = json.load(d)
    for app in data:
        id = app['id']
        name = app['name']
        icon = app['icon']
        description = app['description']
        resources = json.dumps(app['resources'])
        developer_name = app['developer_name']
        screenshots = json.dumps(app['screenshots'])
        category_name = app['category']

        ## Check if category already exist in DB
        catquery = Category.query.filter_by(name=category_name).first()
        if catquery is None:
            cat = Category(
                name=category_name
            )
            db.session.add(cat)
        else:
            cat = Category.query.filter_by(id=catquery.id).first()
            

        game = Games(
            id=id,
            name=name,
            icon = icon,
            description = description,
            resources = resources,
            developer_name = developer_name,
            screenshots = screenshots,
            category = cat
        )
        db.session.add(game)
        db.session.commit()
        print("App Data")

@app.route("/")
def home():
    return render_template("home.html")

@app.route("/app/", defaults={'page_num': 1})
@app.route("/app/<int:page_num>")
def index(page_num):
    # games = Games.query.all()
    games = Games.query.paginate(per_page=9, page=page_num)
    return render_template("index.html", games=games, current_page=page_num)

@app.route("/all-category")
def allcategory():
    categories = Category.query.all()
    return render_template("category.html", categories=categories)


@app.route("/category/<cat_name>", defaults={'page_num': 1})
@app.route("/category/<cat_name>/<int:page_num>")
def category(cat_name, page_num):
    catdata = Category.query.filter_by(name=cat_name).first()
    games = Games.query.filter_by(category_id=catdata.id).paginate(per_page=3, page=page_num, error_out=True)
    return render_template("single-cat.html", catdata=catdata, games=games, cat_name=cat_name)

@app.route("/<game_id>")
def game(game_id):
    gamedata = Games.query.filter_by(id=game_id).first()
    return render_template("game.html", gamedata=gamedata)


if __name__ == "__main__":
    app.run(debug=True)